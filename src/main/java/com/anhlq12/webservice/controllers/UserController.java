package com.anhlq12.webservice.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.anhlq12.webservice.constants.Constant;
import com.anhlq12.webservice.entities.User;
import com.anhlq12.webservice.services.WSController;
import com.anhlq12.webservice.utils.JSONUtil;
import com.anhlq12.webservice.utils.Utilities;

public class UserController {

	private WSController ws;
	private Connection conn;

	public UserController() {
		ws = new WSController();
	}

	public UserController(Connection conn) {
		ws = new WSController();
		this.conn = conn;
	}

	// ==================== WEB SERVICE ======================
	public List<User> getAllWS() {
		String response = ws.get(Constant.GET_ALL_USERS_URL);
		System.out.println(response);
		if (response == null)
			return null;
		StringBuffer buffer = new StringBuffer();
		int total = Integer.parseInt(JSONUtil.getNodeValue(response, "pages").get(0));
		List<User> users = new ArrayList<User>();
		for (int i = 1; i <= total; i++) {
			buffer.append(Constant.GET_ALL_USERS_URL).append("?page=").append(i);
//			System.out.println(buffer.toString());
			response = ws.get(buffer.toString());
			if (response == null) {
				System.out.println("Cannot get users at page = " + i);
				continue;
			}
			List<String> data = JSONUtil.getNodeValue(response, "data");
			for (int j = 0; j < data.size(); j++) {
				String json = data.get(j).replace("created_at", "createdAt").replace("updated_at", "updatedAt");
				users.add((User) JSONUtil.toObject(json, "User"));
			}
			buffer.setLength(0); // clear buffer
		}
		return users;
	}

	public User getUserByIDWS(int id) {
		String response = ws.get(Constant.ENDPOINT + id);
		if (response == null)
			return null;
		User user = null;
		List<String> data = JSONUtil.getNodeValue(response, "data");
		for (int i = 0; i < data.size(); i++) {
			String json = data.get(i).replace("created_at", "createdAt").replace("updated_at", "updatedAt");
			user = (User) JSONUtil.toObject(json, "User");
		}
		return user;
	}

	public User createUserWS(User user) {
		String response = ws.create(Constant.ENDPOINT, user);
		if (response == null)
			return null;
		String jsonUser = JSONUtil.getNodeValue(response, "data").get(0);
		jsonUser = jsonUser.replace("created_at", "createdAt").replace("updated_at", "updatedAt");
		return (User) JSONUtil.toObject(jsonUser, "User");
	}

	public boolean deleleUserWS(int id) {
		String resourceURL = Constant.ENDPOINT + id;
		User user = getUserByIDWS(id);
		if (user == null)
			return false;
		String response = ws.delete(resourceURL);
		if (response == null)
			return false;
		return true;
	}

	public boolean updateUserWS(User user) {
		if (user == null)
			return false;
		String resourceURL = Constant.ENDPOINT + user.getId();
		String response = ws.update(resourceURL, user);
		System.out.println(response);
		if (response == null)
			return false;
		return true;
	}

	// ======================= DATABASE ============================
	public User insert(User user) {
		if (hasUser(user)) {
			System.err.println("The record already exists in database: " + user.getId());
			return null;
		}
		try {
			String query = "INSERT INTO users (user_id, name, email, gender, status, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement stmt = conn.prepareStatement(query);
			if (stmt == null)
				return null;
			stmt.setInt(1, user.getId());
			stmt.setString(2, user.getName());
			stmt.setString(3, user.getEmail());
			stmt.setString(4, user.getGender());
			stmt.setString(5, user.getStatus());
			stmt.setString(6, user.getCreatedAt());
			stmt.setString(7, user.getUpdatedAt());
			if (stmt.executeUpdate() >= 1)
				return user;
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("An unexpected error during inserting data to database: User ID = " + user.getId());
		}
		return null;
	}

	public boolean insert(List<User> users) {
		if (users == null) {
			return false;
		}
		boolean isOK = true;
		for (User user : users) {
			User u = insert(user);
			if (u == null) {
				isOK = false;
			}
		}
		return isOK;
	}

	public boolean delete(User user) {
		try {
			String query = "DELETE FROM users WHERE user_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			if (stmt == null)
				return false;
			stmt.setInt(1, user.getId());
			return stmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean update(User user) {
		try {
			String query = "UPDATE users SET name = ?, email = ?, gender = ?, status = ?, updated_at = ? WHERE user_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			if (stmt == null)
				return false;
			stmt.setString(1, user.getName());
			stmt.setString(2, user.getEmail());
			stmt.setString(3, user.getGender());
			stmt.setString(4, user.getStatus());
			stmt.setString(5, Utilities.getCurrent());
			stmt.setInt(6, user.getId());
			return stmt.executeUpdate() >= 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean hasUser(User user) {
		try {
			String query = "SELECT count(*) FROM users WHERE user_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			if (stmt == null)
				return false;
			stmt.setInt(1, user.getId());
			ResultSet rs = stmt.executeQuery();
			rs.next();
			return rs.getInt(1) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public User getUserByIDDB(int id) {
		try {
			String query = "SELECT * FROM users WHERE user_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			if (stmt == null)
				return null;
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			if (!rs.next()) {
				return null;
			}
			return new User(rs.getInt("user_id"), rs.getString("name"), rs.getString("email"), rs.getString("gender"),
					rs.getString("status"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<User> getAllDB() {
		List<User> users = new ArrayList<User>();
		User u = null;
		try {
			String query = "SELECT * FROM users";
			PreparedStatement stmt = conn.prepareStatement(query);
			if (stmt == null)
				return null;
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				int userID = rs.getInt("user_id");
				String name = rs.getString("name");
				String email = rs.getString("email");
				String gender = rs.getString("gender");
				String status = rs.getString("status");
				u = new User(userID, name, email, gender, status);
				users.add(u);
			}
			return users;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void sync() {
		List<User> ws = getAllWS();
		List<User> db = getAllDB();
		List<Integer> wsIds = getIDs(ws);
		List<Integer> dbIds = getIDs(db);
		List<Integer> common = Utilities.getCommonFromLists(wsIds, dbIds);
		List<Integer> thisIds = Utilities.getThis(wsIds, dbIds);
		List<Integer> thatIds = Utilities.getThat(wsIds, dbIds);
		for (int id : common) {
			User userWS = ws.get(wsIds.indexOf(id));
			User userDB = getUserByIDDB(id);
			System.out.println(userWS);
			System.out.println(userDB);
			if (userWS != null && userDB != null && userWS.compareTo(userDB) != 0) {
				update(userWS);
				System.out.println("Update: " + userWS.getId());
			}
		}

		if (thisIds != null) {
			for (int id : thisIds) {
				User userWS = ws.get(wsIds.indexOf(id));
				User userDB = getUserByIDDB(id);
				if (userWS != null && userDB == null) {
					insert(userWS);
					System.out.println("Insert: " + userWS.getId());
				}
			}
		}

		if (thatIds != null) {
			for (int id : thatIds) {
				User userWS = ws.get(wsIds.indexOf(id));
				User userDB = getUserByIDDB(id);
				if (userWS != null && userDB != null && userWS.compareTo(userDB) != 0) {
					delete(userDB);
					System.out.println("Delete: " + userDB.getId());
				}
			}
		}
	}

	public List<Integer> getIDs(List<User> users) {
		List<Integer> ids = new ArrayList<Integer>();
		for (User u : users) {
			ids.add(u.getId());
		}
		return ids;
	}
}
