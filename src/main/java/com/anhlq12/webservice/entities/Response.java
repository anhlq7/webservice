package com.anhlq12.webservice.entities;

import java.util.ArrayList;
import java.util.List;

public class Response {
	
	private Code code;
	private Meta meta;
	private List<User> users;
	
	public Response() {
		code = new Code();
		meta = new Meta();
		users = new ArrayList<User>();
	}
	
}
