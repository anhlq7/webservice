package com.anhlq12.webservice.entities;

import com.anhlq12.webservice.utils.Utilities;

public class User implements Comparable{
	
	//{"id":21,"name":"Ghanshyam Nayar","email":"nayar_ghanshyam@schoen.org","gender":"Female","status":"Active","created_at":"2020-09-15T03:50:04.601+05:30","updated_at":"2020-09-15T03:50:04.601+05:30"}
	
	private int id;
	private String name;
	private String email;
	private String gender;
	private String status;
	private String createdAt;
	private String updatedAt;
	
	public User() {
		
	}
	
	public User(String name, String email, String gender, String status) {
		this.name = name;
		this.email = email;
		this.gender = gender;
		this.status = status;
	}
	
	public User(int id, String name, String email, String gender, String status) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.gender = gender;
		this.status = status;
	}
	
	public User(int id, String name, String email, String gender, String status, String createdAt, String updatedAt) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.gender = gender;
		this.status = status;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createAt) {
		this.createdAt = createAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updateAt) {
		this.updatedAt = updateAt;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void printOutTableFormat() {
		System.out.format("%-10d%-40s%-50s%-10s%-35s%-35s", id, name, email, gender, createdAt, updatedAt);
		System.out.println();
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", email=" + email + ", gender=" + gender + ", createdAt="
				+ createdAt + ", updatedAt=" + updatedAt + "]";
	}

	public int compareTo(Object o) {
		User u = (User) o;
		if(u.getId() == id && u.getName().equals(name) && u.getEmail().equals(email) && u.getGender().equals(gender) && u.getStatus().equals(status)) {
			return 0;
		}
		return -1;
	}
}
