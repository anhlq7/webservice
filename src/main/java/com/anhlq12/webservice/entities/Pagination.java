package com.anhlq12.webservice.entities;

public class Pagination {
	
	private int total;
	private int pages;
	private int page;
	private int limit;
	
	public Pagination() {
		
	}
	
	public Pagination(int total, int page, int limit) {
		this.total = total;
		this.limit = limit;
		this.page = page;
		this.pages = Math.round(total/limit);
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		return "Pagination [total=" + total + ", pages=" + pages + ", page=" + page + ", limit=" + limit + "]";
	}
}
