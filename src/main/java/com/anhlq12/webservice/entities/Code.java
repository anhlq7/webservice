package com.anhlq12.webservice.entities;

public class Code {
	
	private int code;
	
	public Code() {
		
	}
	
	public Code(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "Code [code=" + code + "]";
	}
}
