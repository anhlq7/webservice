package com.anhlq12.webservice.entities;

public class Meta {
	
	private Pagination pagination;
	
	public Meta() {
		
	}
	
	public Meta(Pagination pagination) {
		this.pagination = pagination;
	}
	
}
