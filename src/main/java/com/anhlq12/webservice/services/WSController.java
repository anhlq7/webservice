package com.anhlq12.webservice.services;

import java.util.Arrays;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.anhlq12.webservice.constants.Constant;
import com.anhlq12.webservice.utils.JSONUtil;

public class WSController {

	private RestTemplate restTemplate;
	private HttpHeaders headers;
	private HttpEntity<String> entity;

	public WSController() {
		restTemplate = new RestTemplate();
		headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("authorization", "Bearer " + Constant.TOKEN);
		entity = new HttpEntity<String>(headers);
	}

	public String get(String resourceURL) {
		ResponseEntity<String> response = restTemplate.exchange(resourceURL, HttpMethod.GET, entity, String.class);
		if (!validate(response)) {
			return null;
		}
		return response.getBody();
	}

	public String create(String resourceURL, Object o) {
		HttpEntity<Object> entity = new HttpEntity<Object>(o, headers);
		ResponseEntity<String> response = restTemplate.exchange(resourceURL, HttpMethod.POST, entity, String.class);
		if (!validate(response)) {
			return null;
		}
		return response.getBody();
	}

	public String delete(String resourceURL) {
		ResponseEntity<String> response = restTemplate.exchange(resourceURL, HttpMethod.DELETE, entity, String.class);
		if (!validate(response)) {
			return null;
		}
		return response.getBody();
	}

	public String update(String resourceURL, Object o) {
		HttpEntity<Object> entity = new HttpEntity<Object>(o, headers);
		ResponseEntity<String> response = restTemplate.exchange(resourceURL, HttpMethod.PUT, entity, String.class);
		if (!validate(response)) {
			return null;
		}
		return response.getBody();
	}

	/**
	 * 
	 * 
	 * 500: Internal server error. This could be caused by internal program errors.
	 * 
	 * @param response
	 * @return
	 */
	public boolean validate(ResponseEntity<String> response) {
		StringBuffer buffer = new StringBuffer();
		String json = response.getBody();
		int code = Integer.parseInt(JSONUtil.getNodeValue(json, "code").get(0));
		switch (code) {
		case 200:
		case 201:
		case 204:
			return true;
		case 304:
		case 400:
		case 401:
		case 403:
		case 404:
		case 405:
		case 415:
		case 429:
		case 500:
			return false;
		case 422:
			if (JSONUtil.getNodeValue(json, "field").get(0) != null) {
				buffer.append(JSONUtil.getNodeValue(json, "field").get(0)).append(": ")
						.append(JSONUtil.getNodeValue(json, "message").get(0));
			} else {
				buffer.append(": ").append(JSONUtil.getNodeValue(json, "message").get(0));
			}
			System.err.println(buffer.toString());
			System.out.println();
			return false;
		}
		return false;
	}
}
