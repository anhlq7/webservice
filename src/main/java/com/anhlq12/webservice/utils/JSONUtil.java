package com.anhlq12.webservice.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

import com.anhlq12.webservice.constants.Constant;

public class JSONUtil {

	private static ObjectMapper mapper = new ObjectMapper();
	

	public static JsonNode readTree(String json) {
		try {
			return mapper.readTree(json);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<String> traverseJsonArray(String json, String nodename) {
		List<String> list = new ArrayList<String>();
		JsonNode jsonNode = readTree(json);
		ArrayNode arrayNode = (ArrayNode) (jsonNode.findPath(nodename));
		for (int i = 0; i < arrayNode.size(); i++) {
			Iterator<Map.Entry<String, JsonNode>> it = arrayNode.get(i).getFields();
			list.add(toJSONString(it));
		}
		return list;
	}

	public static List<String> traverseContainerNode(String json, String nodename) {
		List<String> list = new ArrayList<String>();
		JsonNode jsonNode = readTree(json);
		if (jsonNode == null)
			return null;
		Iterator<Map.Entry<String, JsonNode>> it = jsonNode.findPath(nodename).getFields();
		list.add(JSONUtil.toJSONString(it));
		return list;
	}

	public static List<String> traverseValueNode(String json, String nodename) {
		List<String> list = new ArrayList<String>();
		JsonNode jsonNode = readTree(json);
		if (jsonNode == null)
			return null;
		list.add(jsonNode.findPath(nodename).asText());
		return list;
	}

	public static List<String> getNodeValue(String json, String nodename) {
		JsonNode jsonNode = readTree(json);
		if (jsonNode.findPath(nodename).isArray())
			return traverseJsonArray(json, nodename);
		if (jsonNode.findPath(nodename).isContainerNode())
			return traverseContainerNode(json, nodename);
		if (jsonNode.findPath(nodename).isValueNode())
			return traverseValueNode(json, nodename);
		return null;
	}

	public static String toJSONString(Iterator<Map.Entry<String, JsonNode>> it) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("{");
		while (it.hasNext()) {
			Map.Entry<String, JsonNode> f = it.next();
			buffer.append("\"").append(f.getKey()).append("\"").append(":").append("\"").append(f.getValue().asText())
					.append("\"").append(",");
		}
		buffer.deleteCharAt(buffer.lastIndexOf(","));
		buffer.append("}");
		return buffer.toString();
	}

	public static Object toObject(String json, String classname) {
		try {
			return mapper.readValue(json, Class.forName(Constant.URL_ENTITIES + "." + classname));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
