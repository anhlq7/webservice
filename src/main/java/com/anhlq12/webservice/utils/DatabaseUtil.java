package com.anhlq12.webservice.utils;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseUtil {

	public static Connection getConnection(String dbURL, String userName, String password) {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(dbURL, userName, password);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return conn;
	}
}
