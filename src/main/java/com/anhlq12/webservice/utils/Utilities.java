package com.anhlq12.webservice.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Utilities {

	private static final String DIR_PROJECT = System.getProperty("user.dir");
	private static final String DIR_CONFIG = DIR_PROJECT + "\\" + "config.properties";

	public static String getProperty(String key) {
		Properties p = new Properties();
		FileInputStream fis;
		try {
			fis = new FileInputStream(DIR_CONFIG);
			p.load(fis);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return p.getProperty(key);
	}

	public static String capitalize(String str) {
		if (str == null || str.isEmpty()) {
			return str;
		}

		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}

	public static String getCurrent() {
		return java.time.LocalDateTime.now().toString();
	}

	public static List<Integer> getCommonFromLists(List<Integer> thisList, List<Integer> thatList) {
		if(thisList == null || thisList.isEmpty()) {
			return null;
		}
		if(thatList == null || thatList.isEmpty()) {
			return null;
		}
		List<Integer> common = (thisList.size() >= thatList.size()) ? new ArrayList<Integer>(thisList)
				: new ArrayList<Integer>(thatList);
		boolean b = (thisList.size() >= thatList.size()) ? common.retainAll(thatList) : common.retainAll(thisList);
		return common;
	}

	public static List<Integer> getThis(List<Integer> thisList, List<Integer> thatList) {
		List<Integer> common = getCommonFromLists(thisList, thatList);
		if (common == null)
			return null;
		thisList.removeAll(common);
		return thisList;
	}

	public static List<Integer> getThat(List<Integer> thisList, List<Integer> thatList) {
		List<Integer> common = getCommonFromLists(thisList, thatList);
		if (common == null)
			return null;
		thatList.removeAll(common);
		return thatList;
	}

}
