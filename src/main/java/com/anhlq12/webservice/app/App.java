package com.anhlq12.webservice.app;

import java.sql.Connection;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import com.anhlq12.webservice.controllers.UserController;
import com.anhlq12.webservice.entities.User;
import com.anhlq12.webservice.utils.DatabaseUtil;
import com.anhlq12.webservice.utils.Utilities;

public class App {

	private static String server = Utilities.getProperty("server");
	private static String hostname = Utilities.getProperty("hostname");
	private static String port = Utilities.getProperty("port");
	private static String username = Utilities.getProperty("username");
	private static String password = Utilities.getProperty("password");
	private static String databaseName = Utilities.getProperty("databaseName");
	private static final List<String> USER_INFO = Arrays.asList("Name", "Email", "Gender", "Status");

	public static void main(String[] args) {
		String dbURL = "jdbc:" + server + "://" + hostname + ":" + port + ";" + "databaseName=" + databaseName + ";";
		Connection conn = DatabaseUtil.getConnection(dbURL, username, password);
		UserController cUser = new UserController(conn);
		Scanner input = new Scanner(System.in);
		boolean cont = true;
		System.out.println("Start to sync..");
		cUser.sync();
		System.out.println("Synced.");
		while (cont) {
			printMenu();
			int choice = choose(input);
			switch (choice) {
			case 1:
				List<User> users = cUser.getAllWS();
				boolean isSuccess = cUser.insert(users);
				if (!isSuccess) {
					System.err.println(
							"An unexpected error during inserting data to database. Cannot fully insert data into database");
					System.out.println();
					continue;
				}
				System.out.println("Successfully retrieved + stored users into database");
				break;
			case 2:
				int id = inputID(input);
				User user = cUser.getUserByIDWS(id);
				if (user == null) {
					System.err.println("The record did not exist. Cannot retrieve");
					System.out.println();
					continue;
				}
				user.printOutTableFormat();
				boolean exist = cUser.hasUser(user);
				if (exist) {
					System.err.println("The record has already been added to database.");
					System.out.println();
					continue;
				}
				isSuccess = (cUser.insert(user) != null);
				if (!isSuccess) {
					System.err.println(
							"Cannot fully insert data into database");
					System.out.println();
					continue;
				}
				System.out.println("Successfully retrieved + stored users into database");
				break;
			case 3:
				String[] info = inputUser(input);
				user = new User(info[0], info[1], info[2], info[3]);
				User gorestUser = cUser.createUserWS(user);
				isSuccess = (gorestUser != null);
				if (!isSuccess) {
					System.err.println("An unexpected error during creating the user");
					System.out.println();
					continue;
				}
				System.out.println("Successfully created the user: " + gorestUser.getId());
				user = new User(gorestUser.getId(),info[0], info[1], info[2], info[3]);
				user.printOutTableFormat();
				break;
			case 4:
				id = inputID(input);
				user = cUser.getUserByIDWS(id);
				user.printOutTableFormat();
				String name = user.getName();
				String email = user.getEmail();
				String gender = user.getGender();
				String status = user.getStatus();
				String v = "";
				for(String f : USER_INFO) {
					int opt = askUpdate(input, f);
					f = f.toLowerCase();
					if(opt == 1) {
						v = inputField(input, f);
					}
					if("name".equals(f)) {
						name = opt == 1 ? v : user.getName();
						continue;
					}
					if("email".equals(f)) {
						email = opt == 1 ? v : user.getEmail();
						continue;
					}
					if("gender".equals(f)) {
						gender = opt == 1 ? v : user.getGender();
						continue;
					}
					if("status".equals(f)) {
						status = opt == 1 ? v : user.getStatus();
						continue;
					}
				}
				user = new User(id, name, email, gender, status);
				cUser.updateUserWS(user);
				cUser.update(cUser.getUserByIDDB(id));
				break;
			case 5:
				id = inputID(input);
				isSuccess = cUser.deleleUserWS(id);
				if (!isSuccess) {
					System.err.println("An unexpected error during deleting the user");
					System.out.println();
					continue;
				}
				cUser.delete(cUser.getUserByIDDB(id));
				System.out.println("Successfully deleted the user: " + id);
				break;
			case 6:
				users = cUser.getAllWS();
				for (User u : users) {
					u.printOutTableFormat();
				}
				break;
			case 7:
				String exit = exit(input);
				if ("no".equals(exit)) {
					continue;
				}
				System.out.println("Exit the program");
				System.exit(0);
				break;
			}
		}
	}

	public static void printMenu() {
		System.out.println("1. Retrieve all users + Store them into DB");
		System.out.println("2. Retrieve user by ID + Store it into DB");
		System.out.println("3. Create an gorest user");
		System.out.println("4. Update an gorest user");
		System.out.println("5. Delete an gorest user");
		System.out.println("6. View all gorest users");
		System.out.println("7. Exit");
	}

	public static int choose(Scanner input) {
		System.out.println("Please enter your option: ");
		int opt = -1;
		while (true) {
			try {
				opt = input.nextInt();
				if (opt <= 7 && opt >= 1) {
					return opt;
				}
			} catch (InputMismatchException e) {

			} finally {
				input.nextLine();
			}
			System.out.println("An invalid option. Please re-enter: ");
		}
	}

	public static int inputID(Scanner input) {
		System.out.println("Please enter UserID: ");
		int id = -1;
		while (true) {
			try {
				id = input.nextInt();
				return id;
			} catch (InputMismatchException e) {

			} finally {
				input.nextLine();
			}
			System.out.println("An invalid option. Please re-enter: ");
		}
	}

	public static String exit(Scanner input) {
		System.out.println("Are you sure that you wanna exit the program?");
		String opt = null;
		while (true) {
			opt = input.nextLine().trim();
			if ("yes".equalsIgnoreCase(opt) || "no".equalsIgnoreCase(opt)) {
				return opt;
			}
			System.out.println("An invalid option. Please re-enter: ");
		}
	}

	public static String[] inputUser(Scanner input) {
		String[] info = new String[USER_INFO.size()];
		int index = 0;
		for (String f : USER_INFO) {
			System.out.println("Please enter <" + f + ">: ");
			info[index] = ("Gender".equals(f) || "Status".equals(f)) ? Utilities.capitalize(input.nextLine())
					: input.nextLine();
			index++;
		}
		return info;
	}
	
	public static String inputField(Scanner input, String f) {
		System.out.println("Enter <" + f + ">: ");
		return input.nextLine();
	}
	
	public static int askUpdate(Scanner input, String f) {
		String opt = null;
		while(true) {
			System.out.println("Do you want to update <" + f + ">? ");
			opt = input.nextLine().toLowerCase();
			if(!"yes".equals(opt) && !"no".equals(opt)) {
				System.out.println("An invalid option. Please re-enter: ");
				continue;
			}
			return ("yes".equals(opt)) ? 1 : 0;
		}
	}
}
