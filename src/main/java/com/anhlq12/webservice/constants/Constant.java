package com.anhlq12.webservice.constants;

public class Constant {
	
	public static final String GET_ALL_USERS_URL = "https://gorest.co.in/public-api/users";
	
	public static final String ENDPOINT = "https://gorest.co.in/public-api/users/";
	
	public static final String URL_ENTITIES = "com.anhlq12.webservice.entities";
	
	public static final String TOKEN = "46a64a9ff7563a37dbdd24c53c7a0fa2d10e8cf3ba128bc01704674dea13c735";
}
